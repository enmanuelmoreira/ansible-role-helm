# Ansible Role: Helm

This role installs the [Helm](https://helm.sh) binary on any supported host.

## Requirements

N/A

## Installing

The role can be installed by running the following command:

```bash
git clone https://gitlab.com/enmanuelmoreira/ansible-role-helm enmanuelmoreira.helm
```

Add the following line into your `ansible.cfg` file:

```bash
[defaults]
roles_path = ../
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    helm_version: 'v3.8.1'
    helm_platform: linux
    helm_arch: amd64

The path to the main Helm repo. Unlessy you need to override this for special reasons (e.g. running on servers without public Internet access), you should leave it as the default.

    helm_bin_path: /usr/local/bin/helm

The location where the Helm binary will be installed.

Specific vars for Debian/Ubuntu are into `vars/Debian.yml` file.  

    helm_package_name: helm
    helm_gpg_key: "https://baltocdn.com/helm/signing.asc"
    helm_repo_path: "deb https://baltocdn.com/helm/stable/debian/ all main"

Controls for the version of Helm to be installed. See [available Helm releases](https://github.com/helm/helm/releases/). You can upgrade or downgrade versions by changing the `helm_version` into `vars/Other.yml` file.

    helm_repo_path: "https://get.helm.sh"

## Tested on

* Ubuntu 20.04
* Ubuntu 18.04
* Rocky 8
* MacOS Monterrey

## Dependencies

None.

## Example Playbook

    - hosts: all
      roles:
        - role: enmanuelmoreira.helm

## License

MIT / BSD
